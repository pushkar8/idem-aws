import copy
import time
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_resource(hub, ctx, aws_apigateway_rest_api):
    rest_api_id = aws_apigateway_rest_api.get("resource_id")
    name = "idem-test-resource-" + str(int(time.time()))
    update_name = "idem-test-resource-name-update-" + str(int(time.time()))
    path_part = "idem-test-resource-name-" + str(int(time.time()))

    before = await hub.exec.boto3.client.apigateway.get_resources(
        ctx, restApiId=rest_api_id
    )
    parent_id = before["ret"]["items"][0].get("id")

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create with Test Flag
    present_ret = await hub.states.aws.apigateway.resource.present(
        test_ctx,
        name=name,
        rest_api_id=rest_api_id,
        parent_id=parent_id,
        path_part=path_part,
    )

    assert present_ret["result"], present_ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_create_comment(
            resource_type="aws.apigateway.resource", name=name
        )[0]
        in present_ret["comment"]
    )
    assert not present_ret.get("old_state") and present_ret.get("new_state")
    resource = present_ret.get("new_state")
    assert name == resource.get("name")
    assert rest_api_id == resource.get("rest_api_id")
    assert parent_id == resource.get("parent_id")
    assert path_part == resource.get("path_part")

    # Real Create
    present_ret = await hub.states.aws.apigateway.resource.present(
        ctx,
        name=name,
        rest_api_id=rest_api_id,
        parent_id=parent_id,
        path_part=path_part,
    )

    assert not present_ret.get("old_state") and present_ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.create_comment(
            resource_type="aws.apigateway.resource", name=name
        )[0]
        in present_ret["comment"]
    )
    resource = present_ret["new_state"]
    resource_id = resource["resource_id"]
    assert resource["name"] == resource.get("name")
    assert rest_api_id == resource.get("rest_api_id")
    assert parent_id == resource.get("parent_id")
    assert path_part == resource.get("path_part")

    # Describe Resource
    ret = await hub.states.aws.apigateway.resource.describe(ctx)
    resource = ret.get(resource_id).get("aws.apigateway.resource.present")
    resource_map = dict(ChainMap(*resource))
    assert resource_id == resource_map.get("resource_id")
    assert rest_api_id == resource_map.get("rest_api_id")
    assert parent_id == resource_map.get("parent_id")
    assert path_part == resource_map.get("path_part")

    # Create Again with same resource_id and no changes with Test Flag
    ret = await hub.states.aws.apigateway.resource.present(
        test_ctx,
        name=name,
        rest_api_id=rest_api_id,
        resource_id=resource_id,
        parent_id=parent_id,
        path_part=path_part,
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigateway.resource",
            name=name,
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Create again with same resource_id and no change in Real
    ret = await hub.states.aws.apigateway.resource.present(
        ctx,
        name=name,
        rest_api_id=rest_api_id,
        resource_id=resource_id,
        parent_id=parent_id,
        path_part=path_part,
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigateway.resource",
            name=name,
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Update Real
    ret = await hub.states.aws.apigateway.resource.present(
        ctx,
        name=name,
        resource_id=resource_id,
        rest_api_id=rest_api_id,
        parent_id=parent_id,
        path_part=update_name,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret["new_state"]
    assert name == resource["name"]
    assert update_name == resource["path_part"]
    assert (
        hub.tool.aws.comment_utils.update_comment(
            resource_type="aws.apigateway.resource",
            name=name,
        )[0]
        in ret["comment"]
    )

    # Delete Instance with Test Flag
    ret = await hub.states.aws.apigateway.resource.absent(
        test_ctx, name=name, rest_api_id=rest_api_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.apigateway.resource",
            name=name,
        )[0]
        in ret["comment"]
    )

    # Delete Instance - Real
    ret = await hub.states.aws.apigateway.resource.absent(
        ctx, name=name, rest_api_id=rest_api_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert update_name == resource.get("name")
    assert rest_api_id == resource.get("rest_api_id")
    assert resource_id == resource.get("resource_id")
    assert parent_id == resource.get("parent_id")
    assert update_name == resource.get("path_part")
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.apigateway.resource",
            name=name,
        )[0]
        in ret["comment"]
    )

    # Delete Instance Again
    ret = await hub.states.aws.apigateway.resource.absent(
        ctx, name=name, rest_api_id=rest_api_id, resource_id=resource_id
    )
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigateway.resource",
            name=name,
        )[0]
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])
