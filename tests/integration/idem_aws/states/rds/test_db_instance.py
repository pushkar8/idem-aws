import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(
    False,
    "To skip running on localstack as local stack is not able to create the resource, it is working with real aws",
)
@pytest.mark.asyncio
async def test_db_instance_aurora(hub, ctx, aws_rds_db_cluster):
    # Create db instance for aurora
    db_instance_name = "idem-test-db-instance-" + str(uuid.uuid4())
    db_instance_class = "db.t3.small"
    engine = "aurora-mysql"
    availability_zone = ctx["acct"].get("region_name") + "a"
    preferred_maintenance_window = "sat:04:15-sat:04:45"
    engine_version = "5.7.mysql_aurora.2.10.2"
    auto_minor_version_upgrade = True
    license_model = "general-public-license"
    publicly_accessible = False
    storage_type = "aurora"
    copy_tags_to_snapshot = False
    db_cluster_identifier = aws_rds_db_cluster.get("resource_id")
    tags = {"Name": db_instance_name}
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    # Create DB Instance with test flags
    ret = await hub.states.aws.rds.db_instance.present(
        test_ctx,
        name=db_instance_name,
        db_instance_class=db_instance_class,
        engine=engine,
        availability_zone=availability_zone,
        preferred_maintenance_window=preferred_maintenance_window,
        engine_version=engine_version,
        auto_minor_version_upgrade=auto_minor_version_upgrade,
        license_model=license_model,
        publicly_accessible=publicly_accessible,
        storage_type=storage_type,
        copy_tags_to_snapshot=copy_tags_to_snapshot,
        db_cluster_identifier=db_cluster_identifier,
        tags=tags,
    )
    assert f"Would create aws.rds.db_instance '{db_instance_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_db_instance_aurora(
        resource,
        db_instance_name,
        db_instance_class,
        engine,
        availability_zone,
        preferred_maintenance_window,
        engine_version,
        auto_minor_version_upgrade,
        license_model,
        publicly_accessible,
        storage_type,
        copy_tags_to_snapshot,
        db_cluster_identifier,
        tags,
    )

    # Create DB Instance
    ret = await hub.states.aws.rds.db_instance.present(
        ctx,
        name=db_instance_name,
        db_instance_class=db_instance_class,
        engine=engine,
        availability_zone=availability_zone,
        preferred_maintenance_window=preferred_maintenance_window,
        engine_version=engine_version,
        auto_minor_version_upgrade=auto_minor_version_upgrade,
        license_model=license_model,
        publicly_accessible=publicly_accessible,
        storage_type=storage_type,
        copy_tags_to_snapshot=copy_tags_to_snapshot,
        db_cluster_identifier=db_cluster_identifier,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_db_instance_aurora(
        resource,
        db_instance_name,
        db_instance_class,
        engine,
        availability_zone,
        preferred_maintenance_window,
        engine_version,
        auto_minor_version_upgrade,
        license_model,
        publicly_accessible,
        storage_type,
        copy_tags_to_snapshot,
        db_cluster_identifier,
        tags,
    )
    resource_id = resource.get("resource_id")

    # Describe db instance
    describe_ret = await hub.states.aws.rds.db_instance.describe(ctx)
    assert db_instance_name in describe_ret
    assert "aws.rds.db_instance.present" in describe_ret.get(db_instance_name)
    described_resource = describe_ret[db_instance_name].get(
        "aws.rds.db_instance.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert_db_instance_aurora(
        described_resource_map,
        db_instance_name,
        db_instance_class,
        engine,
        availability_zone,
        preferred_maintenance_window,
        engine_version,
        auto_minor_version_upgrade,
        license_model,
        publicly_accessible,
        storage_type,
        copy_tags_to_snapshot,
        db_cluster_identifier,
        tags,
    )

    # Updating the db_instance with test flag
    tags.update(
        {
            f"idem-test-db-instance-key-{str(uuid.uuid4())}": f"idem-test-db-instance-value-{str(uuid.uuid4())}",
        }
    )
    preferred_maintenance_window = "fri:09:29-fri:09:59"
    db_instance_class = "db.r5.large"

    ret = await hub.states.aws.rds.db_instance.present(
        test_ctx,
        name=db_instance_name,
        resource_id=resource_id,
        db_instance_class=db_instance_class,
        engine=engine,
        availability_zone=availability_zone,
        preferred_maintenance_window=preferred_maintenance_window,
        engine_version=engine_version,
        auto_minor_version_upgrade=auto_minor_version_upgrade,
        license_model=license_model,
        publicly_accessible=publicly_accessible,
        storage_type=storage_type,
        copy_tags_to_snapshot=copy_tags_to_snapshot,
        db_cluster_identifier=db_cluster_identifier,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_db_instance_aurora(
        resource,
        db_instance_name,
        db_instance_class,
        engine,
        availability_zone,
        preferred_maintenance_window,
        engine_version,
        auto_minor_version_upgrade,
        license_model,
        publicly_accessible,
        storage_type,
        copy_tags_to_snapshot,
        db_cluster_identifier,
        tags,
    )

    # Updating the db_instance
    ret = await hub.states.aws.rds.db_instance.present(
        ctx,
        name=db_instance_name,
        resource_id=resource_id,
        apply_immediately=True,
        db_instance_class=db_instance_class,
        engine=engine,
        availability_zone=availability_zone,
        preferred_maintenance_window=preferred_maintenance_window,
        engine_version=engine_version,
        auto_minor_version_upgrade=auto_minor_version_upgrade,
        license_model=license_model,
        publicly_accessible=publicly_accessible,
        storage_type=storage_type,
        copy_tags_to_snapshot=copy_tags_to_snapshot,
        db_cluster_identifier=db_cluster_identifier,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_db_instance_aurora(
        resource,
        db_instance_name,
        db_instance_class,
        engine,
        availability_zone,
        preferred_maintenance_window,
        engine_version,
        auto_minor_version_upgrade,
        license_model,
        publicly_accessible,
        storage_type,
        copy_tags_to_snapshot,
        db_cluster_identifier,
        tags,
    )

    # Delete instance with test flag
    ret = await hub.states.aws.rds.db_instance.absent(
        test_ctx,
        name=db_instance_name,
        resource_id=resource_id,
        skip_final_snapshot=True,
    )
    assert ret["result"], ret["comment"]
    assert f"Would delete aws.rds.db_instance '{db_instance_name}'" in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Delete instance
    ret = await hub.states.aws.rds.db_instance.absent(
        ctx, name=db_instance_name, resource_id=resource_id, skip_final_snapshot=True
    )
    assert ret["result"], ret["comment"]
    assert f"Deleted aws.rds.db_instance '{db_instance_name}'" in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.rds.db_instance.absent(
        ctx, name=db_instance_name, resource_id=resource_id, skip_final_snapshot=True
    )
    assert ret["result"], ret["comment"]
    assert f"aws.rds.db_instance '{db_instance_name}' already absent" in ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))


@pytest.mark.localstack(
    False,
    "To skip running on localstack as local stack is not able to create the resource, it is working with real aws",
)
@pytest.mark.asyncio
async def test_db_instance(hub, ctx, aws_rds_db_subnet_group):
    # Create db instance for mysql,oracle and others
    db_instance_name = "idem-test-db-instance-" + str(uuid.uuid4())
    engine = "mysql"
    db_instance_class = "db.r5.large"
    master_username = "user123"
    master_user_password = "abcde123"
    allocated_storage = 10
    availability_zone = ctx["acct"].get("region_name") + "a"
    db_subnet_group_name = aws_rds_db_subnet_group.get("resource_id")
    preferred_maintenance_window = "fri:09:29-fri:09:59"
    backup_retention_period = 7
    preferred_backup_window = "08:02-08:32"
    multi_az = False
    engine_version = "8.0.27"
    auto_minor_version_upgrade = True
    license_model = "general-public-license"
    publicly_accessible = False
    storage_type = "gp2"
    storage_encrypted = False
    copy_tags_to_snapshot = False
    deletion_protection = False
    tags = {"Name": db_instance_name}
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    # Create DB Instance with test flags
    ret = await hub.states.aws.rds.db_instance.present(
        test_ctx,
        name=db_instance_name,
        db_instance_class=db_instance_class,
        engine=engine,
        master_username=master_username,
        master_user_password=master_user_password,
        allocated_storage=allocated_storage,
        availability_zone=availability_zone,
        db_subnet_group_name=db_subnet_group_name,
        preferred_maintenance_window=preferred_maintenance_window,
        backup_retention_period=backup_retention_period,
        preferred_backup_window=preferred_backup_window,
        multi_az=multi_az,
        engine_version=engine_version,
        auto_minor_version_upgrade=auto_minor_version_upgrade,
        license_model=license_model,
        publicly_accessible=publicly_accessible,
        storage_type=storage_type,
        storage_encrypted=storage_encrypted,
        copy_tags_to_snapshot=copy_tags_to_snapshot,
        deletion_protection=deletion_protection,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.rds.db_instance '{db_instance_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_db_instance(
        resource,
        db_instance_name,
        db_instance_class,
        engine,
        master_username,
        allocated_storage,
        availability_zone,
        db_subnet_group_name,
        preferred_maintenance_window,
        backup_retention_period,
        preferred_backup_window,
        multi_az,
        engine_version,
        auto_minor_version_upgrade,
        license_model,
        publicly_accessible,
        storage_type,
        storage_encrypted,
        copy_tags_to_snapshot,
        deletion_protection,
        tags,
    )

    # Create DB Instance
    ret = await hub.states.aws.rds.db_instance.present(
        ctx,
        name=db_instance_name,
        db_instance_class=db_instance_class,
        engine=engine,
        master_username=master_username,
        master_user_password=master_user_password,
        allocated_storage=allocated_storage,
        availability_zone=availability_zone,
        db_subnet_group_name=db_subnet_group_name,
        preferred_maintenance_window=preferred_maintenance_window,
        backup_retention_period=backup_retention_period,
        preferred_backup_window=preferred_backup_window,
        multi_az=multi_az,
        engine_version=engine_version,
        auto_minor_version_upgrade=auto_minor_version_upgrade,
        license_model=license_model,
        publicly_accessible=publicly_accessible,
        storage_type=storage_type,
        storage_encrypted=storage_encrypted,
        copy_tags_to_snapshot=copy_tags_to_snapshot,
        deletion_protection=deletion_protection,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert f"Created aws.rds.db_instance '{db_instance_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_db_instance(
        resource,
        db_instance_name,
        db_instance_class,
        engine,
        master_username,
        allocated_storage,
        availability_zone,
        db_subnet_group_name,
        preferred_maintenance_window,
        backup_retention_period,
        preferred_backup_window,
        multi_az,
        engine_version,
        auto_minor_version_upgrade,
        license_model,
        publicly_accessible,
        storage_type,
        storage_encrypted,
        copy_tags_to_snapshot,
        deletion_protection,
        tags,
    )
    resource_id = resource.get("resource_id")

    # Describe db instance
    describe_ret = await hub.states.aws.rds.db_instance.describe(ctx)
    assert db_instance_name in describe_ret
    assert "aws.rds.db_instance.present" in describe_ret.get(db_instance_name)
    described_resource = describe_ret[db_instance_name].get(
        "aws.rds.db_instance.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert_db_instance(
        described_resource_map,
        db_instance_name,
        db_instance_class,
        engine,
        master_username,
        allocated_storage,
        availability_zone,
        db_subnet_group_name,
        preferred_maintenance_window,
        backup_retention_period,
        preferred_backup_window,
        multi_az,
        engine_version,
        auto_minor_version_upgrade,
        license_model,
        publicly_accessible,
        storage_type,
        storage_encrypted,
        copy_tags_to_snapshot,
        deletion_protection,
        tags,
    )

    # Updating the db_instance with test flag
    tags.update(
        {
            f"idem-test-db-instance-key-{str(uuid.uuid4())}": f"idem-test-db-instance-value-{str(uuid.uuid4())}",
        }
    )
    backup_retention_period = 5
    preferred_backup_window = "08:41-09:11"
    preferred_maintenance_window = "fri:09:29-fri:09:59"
    db_instance_class = "db.r5.large"
    master_user_password = "test1234"

    ret = await hub.states.aws.rds.db_instance.present(
        test_ctx,
        name=db_instance_name,
        resource_id=resource_id,
        db_instance_class=db_instance_class,
        engine=engine,
        master_username=master_username,
        master_user_password=master_user_password,
        allocated_storage=allocated_storage,
        availability_zone=availability_zone,
        preferred_maintenance_window=preferred_maintenance_window,
        backup_retention_period=backup_retention_period,
        preferred_backup_window=preferred_backup_window,
        multi_az=multi_az,
        engine_version=engine_version,
        auto_minor_version_upgrade=auto_minor_version_upgrade,
        license_model=license_model,
        publicly_accessible=publicly_accessible,
        storage_type=storage_type,
        storage_encrypted=storage_encrypted,
        copy_tags_to_snapshot=copy_tags_to_snapshot,
        deletion_protection=deletion_protection,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_db_instance(
        resource,
        db_instance_name,
        db_instance_class,
        engine,
        master_username,
        allocated_storage,
        availability_zone,
        db_subnet_group_name,
        preferred_maintenance_window,
        backup_retention_period,
        preferred_backup_window,
        multi_az,
        engine_version,
        auto_minor_version_upgrade,
        license_model,
        publicly_accessible,
        storage_type,
        storage_encrypted,
        copy_tags_to_snapshot,
        deletion_protection,
        tags,
    )
    # Updating the db_instance
    ret = await hub.states.aws.rds.db_instance.present(
        ctx,
        name=db_instance_name,
        resource_id=resource_id,
        apply_immediately=True,
        db_instance_class=db_instance_class,
        engine=engine,
        master_username=master_username,
        master_user_password=master_user_password,
        allocated_storage=allocated_storage,
        availability_zone=availability_zone,
        preferred_maintenance_window=preferred_maintenance_window,
        backup_retention_period=backup_retention_period,
        preferred_backup_window=preferred_backup_window,
        multi_az=multi_az,
        engine_version=engine_version,
        auto_minor_version_upgrade=auto_minor_version_upgrade,
        license_model=license_model,
        publicly_accessible=publicly_accessible,
        storage_type=storage_type,
        storage_encrypted=storage_encrypted,
        copy_tags_to_snapshot=copy_tags_to_snapshot,
        deletion_protection=deletion_protection,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_db_instance(
        resource,
        db_instance_name,
        db_instance_class,
        engine,
        master_username,
        allocated_storage,
        availability_zone,
        db_subnet_group_name,
        preferred_maintenance_window,
        backup_retention_period,
        preferred_backup_window,
        multi_az,
        engine_version,
        auto_minor_version_upgrade,
        license_model,
        publicly_accessible,
        storage_type,
        storage_encrypted,
        copy_tags_to_snapshot,
        deletion_protection,
        tags,
    )
    # Test deleting tags
    tags.pop("Name", None)
    ret = await hub.states.aws.rds.db_instance.present(
        ctx,
        name=db_instance_name,
        resource_id=resource_id,
        db_instance_class=db_instance_class,
        engine=engine,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert tags == resource.get("tags")

    # Delete instance with test flag
    ret = await hub.states.aws.rds.db_instance.absent(
        test_ctx,
        name=db_instance_name,
        resource_id=resource_id,
        skip_final_snapshot=True,
    )
    assert ret["result"], ret["comment"]
    assert f"Would delete aws.rds.db_instance '{db_instance_name}'" in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Delete instance
    ret = await hub.states.aws.rds.db_instance.absent(
        ctx, name=db_instance_name, resource_id=resource_id, skip_final_snapshot=True
    )
    assert ret["result"], ret["comment"]
    assert f"Deleted aws.rds.db_instance '{db_instance_name}'" in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.rds.db_instance.absent(
        ctx, name=db_instance_name, resource_id=resource_id, skip_final_snapshot=True
    )
    assert ret["result"], ret["comment"]
    assert f"aws.rds.db_instance '{db_instance_name}' already absent" in ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))


def assert_db_instance(
    resource,
    db_instance_name,
    db_instance_class,
    engine,
    master_username,
    allocated_storage,
    availability_zone,
    db_subnet_group_name,
    preferred_maintenance_window,
    backup_retention_period,
    preferred_backup_window,
    multi_az,
    engine_version,
    auto_minor_version_upgrade,
    license_model,
    publicly_accessible,
    storage_type,
    storage_encrypted,
    copy_tags_to_snapshot,
    deletion_protection,
    tags,
):
    assert db_instance_name == resource.get("name")
    assert db_instance_class == resource.get("db_instance_class")
    assert engine == resource.get("engine")
    assert master_username == resource.get("master_username")
    assert allocated_storage == resource.get("allocated_storage")
    assert availability_zone == resource.get("availability_zone")
    assert db_subnet_group_name == resource.get("db_subnet_group_name")
    assert preferred_maintenance_window == resource.get("preferred_maintenance_window")
    assert backup_retention_period == resource.get("backup_retention_period")
    assert preferred_backup_window == resource.get("preferred_backup_window")
    assert multi_az == resource.get("multi_az")
    assert engine_version == resource.get("engine_version")
    assert auto_minor_version_upgrade == resource.get("auto_minor_version_upgrade")
    assert license_model == resource.get("license_model")
    assert publicly_accessible == resource.get("publicly_accessible")
    assert storage_type == resource.get("storage_type")
    assert storage_encrypted == resource.get("storage_encrypted")
    assert copy_tags_to_snapshot == resource.get("copy_tags_to_snapshot")
    assert deletion_protection == resource.get("deletion_protection")
    assert tags == resource.get("tags")


def assert_db_instance_aurora(
    resource,
    db_instance_name,
    db_instance_class,
    engine,
    availability_zone,
    preferred_maintenance_window,
    engine_version,
    auto_minor_version_upgrade,
    license_model,
    publicly_accessible,
    storage_type,
    copy_tags_to_snapshot,
    db_cluster_identifier,
    tags,
):
    assert db_instance_name == resource.get("name")
    assert db_instance_class == resource.get("db_instance_class")
    assert engine == resource.get("engine")
    assert availability_zone == resource.get("availability_zone")
    assert preferred_maintenance_window == resource.get("preferred_maintenance_window")
    assert engine_version == resource.get("engine_version")
    assert auto_minor_version_upgrade == resource.get("auto_minor_version_upgrade")
    assert license_model == resource.get("license_model")
    assert publicly_accessible == resource.get("publicly_accessible")
    assert storage_type == resource.get("storage_type")
    assert copy_tags_to_snapshot == resource.get("copy_tags_to_snapshot")
    assert db_cluster_identifier == resource.get("db_cluster_identifier")
    assert tags == resource.get("tags")
