import copy
import time
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_rule(hub, ctx):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    account_details = await hub.exec.boto3.client.sts.get_caller_identity(ctx)
    acct_num = account_details["ret"]["Account"]
    config_rule_name = "idem-test-config-rule-restricted-common-ports"
    config_recorder_name = "default-test"
    source = {"Owner": "AWS", "SourceIdentifier": "RESTRICTED_INCOMING_TRAFFIC"}
    tags = {"Name": config_rule_name}
    scope = {"ComplianceResourceTypes": ["AWS::EC2::SecurityGroup"]}
    input_parameters = '{"blockedPort1":"20","blockedPort2":"21","blockedPort3":"3389","blockedPort4":"3306","blockedPort5":"4333"}'

    # Create Config rule with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.config.rule.present(
        test_ctx,
        name=config_rule_name,
        source=source,
        scope=scope,
        input_parameters=input_parameters,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.config.rule {config_rule_name}" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert config_rule_name == resource.get("name")
    assert source == resource.get("source")
    assert scope == resource.get("scope")
    assert input_parameters == resource.get("input_parameters")
    assert tags == resource.get("tags")

    # Create config recorder before creating any config rule
    recording_group_to_add = {
        "allSupported": False,
        "includeGlobalResourceTypes": False,
        "resourceTypes": ["AWS::EC2::CustomerGateway", "AWS::EC2::EIP"],
    }
    role_arn = hub.tool.aws.arn_utils.build(
        service="iam",
        account_id=acct_num,
        resource="role/aws-service-role/config.amazonaws.com/AWSServiceRoleForConfig",
    )
    recording = False
    ret = await hub.states.aws.config.config_recorder.present(
        ctx,
        name=config_recorder_name,
        role_arn=role_arn,
        recording=recording,
        recording_group=recording_group_to_add,
    )

    # Create Config rule
    ret = await hub.states.aws.config.rule.present(
        ctx,
        name=config_rule_name,
        source=source,
        scope=scope,
        input_parameters=input_parameters,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert config_rule_name == resource.get("name")
    assert source == resource.get("source")
    assert scope == resource.get("scope")
    assert input_parameters == resource.get("input_parameters")
    assert tags == resource.get("tags")
    resource_id = resource.get("resource_id")
    assert config_rule_name == resource_id

    # Verify present w/o changes does not trigger an update

    ret = await hub.states.aws.config.rule.present(
        ctx,
        name=config_rule_name,
        resource_id=config_rule_name,
        source=source,
        scope=scope,
        input_parameters=input_parameters,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert f"{config_rule_name} already exists" in ret["comment"]
    assert ret["old_state"] == ret["new_state"], "Should not update existing rule"

    # Describe Config rule
    describe_ret = await hub.states.aws.config.rule.describe(ctx)
    assert resource_id in describe_ret
    assert "aws.config.rule.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get("aws.config.rule.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert config_rule_name == described_resource_map.get("name")
    assert scope == described_resource_map.get("scope")
    assert input_parameters == described_resource_map.get("input_parameters")
    assert source == described_resource_map.get("source")
    assert tags == described_resource_map.get("tags")

    scope = {
        "ComplianceResourceTypes": [
            "AWS::EC2::SecurityGroup",
            "AWS::EC2::VPC",
        ]
    }
    tags = {"idem-test-rule-resource": "EC2"}
    input_parameters = '{"blockedPort1":"20","blockedPort2":"3389"}'
    # Update rule with test flag

    ret = await hub.states.aws.config.rule.present(
        test_ctx,
        name=config_rule_name,
        resource_id=config_rule_name,
        source=source,
        scope=scope,
        input_parameters=input_parameters,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert scope == resource.get("scope")
    assert input_parameters == resource.get("input_parameters")
    assert tags == resource.get("tags")

    # Update rule with real

    ret = await hub.states.aws.config.rule.present(
        ctx,
        name=config_rule_name,
        resource_id=config_rule_name,
        source=source,
        scope=scope,
        input_parameters=input_parameters,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert scope == resource.get("scope")
    assert input_parameters == resource.get("input_parameters")
    assert tags == resource.get("tags")

    # Deleting tags
    tags = {}

    ret = await hub.states.aws.config.rule.present(
        ctx,
        name=config_rule_name,
        resource_id=config_rule_name,
        source=source,
        scope=scope,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert scope == resource.get("scope")
    assert tags == resource.get("tags")

    # Delete rule with test flag
    ret = await hub.states.aws.config.rule.absent(
        test_ctx, name=config_rule_name, resource_id=config_rule_name
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.config.rule", name=config_rule_name
        )[0]
        in ret["comment"]
    )

    # Delete Config rule with real
    ret = await hub.states.aws.config.rule.absent(
        ctx, name=config_rule_name, resource_id=config_rule_name
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    for i in range(5):
        # Waiting to delete the rule, it takes about 1 min.
        ret = await hub.exec.boto3.client.config.describe_config_rules(
            ctx, ConfigRuleNames=[resource_id]
        )
        state = (
            ret["ret"]["ConfigRules"][0]["ConfigRuleState"]
            if ret["result"] and ret["ret"]["ConfigRules"]
            else None
        )
        if state and state == "DELETING":
            time.sleep(60)
        else:
            break

    # Delete Config Rule Again
    ret = await hub.states.aws.config.rule.absent(
        ctx, name=config_rule_name, resource_id=config_rule_name
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))

    # delete the config recorder to clean up stuff
    ret = await hub.states.aws.config.config_recorder.absent(
        ctx, name=config_recorder_name, resource_id=config_recorder_name
    )
